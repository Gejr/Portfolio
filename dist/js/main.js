// Get Menu items
const menuBurger = document.querySelector('.menu-burger');
const menu = document.querySelector('.menu');
const menuNav = document.querySelector('.menu-nav');
const menuBranding = document.querySelector('.menu-branding');
const menuPortrait = document.querySelector('.portrait');
const navItems = document.querySelectorAll('.nav-item');

//Initialize menu state
let showMenu = false;

menuBurger.addEventListener('click', toggleMenu);

function toggleMenu() {
	if(!showMenu) {
		menuBurger.classList.add('close');
		menu.classList.add('show');
		menuNav.classList.add('show');
		menuBranding.classList.add('show');
		menuPortrait.classList.add('show');
		navItems.forEach(item => item.classList.add('show'));

		//Set menu state
		showMenu = true;
	} else {
		menuBurger.classList.remove('close');
		menu.classList.remove('show');
		menuNav.classList.remove('show');
		menuBranding.classList.remove('show');
		menuPortrait.classList.remove('show');
		navItems.forEach(item => item.classList.remove('show'));

		//Set menu state
		showMenu = false;
	}
}
